﻿namespace CommandMessageCreator
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.cbxQueueName = new System.Windows.Forms.ComboBox();
			this.tbxBody = new System.Windows.Forms.TextBox();
			this.btnSend = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(9, 7);
			this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(39, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Queue";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(9, 27);
			this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(74, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "MessageBody";
			// 
			// cbxQueueName
			// 
			this.cbxQueueName.Enabled = false;
			this.cbxQueueName.FormattingEnabled = true;
			this.cbxQueueName.Items.AddRange(new object[] {
            "testcommandhandlerqueue"});
			this.cbxQueueName.Location = new System.Drawing.Point(52, 5);
			this.cbxQueueName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.cbxQueueName.Name = "cbxQueueName";
			this.cbxQueueName.Size = new System.Drawing.Size(152, 21);
			this.cbxQueueName.TabIndex = 2;
			// 
			// tbxBody
			// 
			this.tbxBody.AcceptsReturn = true;
			this.tbxBody.AcceptsTab = true;
			this.tbxBody.Enabled = false;
			this.tbxBody.Location = new System.Drawing.Point(11, 43);
			this.tbxBody.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.tbxBody.Multiline = true;
			this.tbxBody.Name = "tbxBody";
			this.tbxBody.Size = new System.Drawing.Size(192, 130);
			this.tbxBody.TabIndex = 3;
			this.tbxBody.Text = "THIS IS NOT IMPLEMENTED YET. just hit send";
			// 
			// btnSend
			// 
			this.btnSend.Location = new System.Drawing.Point(146, 177);
			this.btnSend.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.btnSend.Name = "btnSend";
			this.btnSend.Size = new System.Drawing.Size(56, 19);
			this.btnSend.TabIndex = 4;
			this.btnSend.Text = "Send";
			this.btnSend.UseVisualStyleBackColor = true;
			this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(212, 206);
			this.Controls.Add(this.btnSend);
			this.Controls.Add(this.tbxBody);
			this.Controls.Add(this.cbxQueueName);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox cbxQueueName;
		private System.Windows.Forms.TextBox tbxBody;
		private System.Windows.Forms.Button btnSend;
	}
}

