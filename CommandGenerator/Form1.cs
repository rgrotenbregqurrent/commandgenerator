﻿using System;
using System.Windows.Forms;
using Autofac;
using Cqrs.Commands;
using Microsoft.Azure;
using Qurrent.Cqrs;
using Qurrent.Cqrs.Autofac;
using Qurrent.Cqrs.AzureServiceBus;
using Qurrent.Cqrs.Commands.DataPortability;
using Qurrent.Domain;

namespace CommandMessageCreator
{
	public partial class Form1 : Form
	{
		private readonly Cqrs.Commands.ICommandPublisher<string> _commandPublisher;

		public Form1()
		{
			InitializeComponent();
			var container = ConfigureDependencies();

			_commandPublisher = container.Resolve<Cqrs.Commands.ICommandPublisher<string>>();
		}

		private void btnSend_Click(object sender, EventArgs e)
		{
			var command = new StartDataCollectionCommand
			{
				AccountId= new AccountId(100),
				BpmId = DateTime.Now.Millisecond,
				Id = Guid.NewGuid()
			};
			_commandPublisher.Publish(command);
			MessageBox.Show("Message has been sent");
		}

		private static IContainer ConfigureDependencies()
		{
			var builder = new ContainerBuilder();
			builder.RegisterModule<BaseCqrsRegistrationModule>();
			builder.RegisterModule<QueueServiceBusRegistrationModule>();

			return builder.Build();
		}
	}
}
